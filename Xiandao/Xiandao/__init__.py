from tcod.console import Console
import tcod
from typing import List
from Xiandao.events import EventHandler, MovementAction
import math

WIDTH, HEIGHT = 720, 480
FLAGS = tcod.context.SDL_WINDOW_RESIZABLE | tcod.context.SDL_WINDOW_MAXIMIZED

console: Console = Console(WIDTH,HEIGHT,order='C')
from Xiandao import Material
from Xiandao.Objects import Object

materialRegistry: List[Material.Material]

def main():
    tileset = tcod.tileset.load_tilesheet(
        "dejavu10x10_gs_tc.png", 32, 8, tcod.tileset.CHARMAP_TCOD,
    )
    event_handler = EventHandler()
    player = Object.Object(10,10,'@',(255,255,255))
    with tcod.context.new(width=WIDTH,height=HEIGHT,sdl_window_flags=FLAGS,title="仙道",tileset=tileset) as context:
        while True:
            console = context.new_console(order="C")
            player.draw(console)
            context.present(console,integer_scaling=True)
            console.clear()
            for event in tcod.event.wait():
                context.convert_event(event)
                action = event_handler.dispatch(event)
                if action is None:
                    continue
                if isinstance(action, MovementAction):
                    player.move(action.dx,action.dy)