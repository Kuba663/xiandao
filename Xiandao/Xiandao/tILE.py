from enum import Enum
from Xiandao.Material import Material, MaterialType
import random
from typing import Tuple

class TileType(Enum):
    NONE = 0
    FOLIAGE = 1
    FLOOR = 2
    WALL_NATURAL = 3
    WALL_ARTIFICIAL = 4
    INACCESIBLE = 5
    WATER = 6
    RAMP = 7
    STAIRS_UP = 8
    STAIRS_UP_DOWN = 8
    STAIRS_DOWN = 10
    pass

class Tile(object):
    """A Tile on the map"""
    def __init__(self,ttype: TileType, material: Material):
        self.tType = ttype
        self.material = material
        if self.tType == TileType.FOLIAGE or (self.tType == TileType.FLOOR and self.material.matType == MaterialType.SOIL):
            self.foliagetype = bool(random.getrandbits(1))
            pass
        pass
    @property
    def char(self) -> str:
        if self.tType == TileType.FOLIAGE:
            return '.' if self.foliagetype else ','
        elif self.tType == TileType.FLOOR:
            if self.material.matType == MaterialType.SOIL:
                return '≈'
            elif self.material.matType == MaterialType.STONE or self.material.matType == MaterialType.WOOD:
                return '+'
            elif self.material.matType == MaterialType.METAL:
                return '≡'
            elif self.material.matType == MaterialType.CRYSTAL:
                return '■'
            elif self.material.matType == MaterialType.LEAVES:
                return '*'
            else:
                raise RuntimeError('Unknown Material Type')
        elif self.tType == TileType.WALL_NATURAL:
            if self.material.matType == MaterialType.SOIL:
                return '▒'
            elif self.material.matType == MaterialType.STONE:
                return '█'
            elif self.material.matType == MaterialType.WOOD:
                return 'O'
            else:
                raise RuntimeError('Unknown or unusable Material Type')
        elif self.tType == TileType.RAMP:
            return '▲'
        elif self.tType == TileType.WATER:
            return '≈'
        elif self.tType == TileType.STAIRS_UP:
            return '>'
        elif self.tType == TileType.STAIRS_UP_DOWN:
            return 'X'
        elif self.tType == TileType.STAIRS_DOWN:
            '<'
        elif self.tType == TileType.INACCESIBLE:
            return ' '
        else:
            return None
        pass
    @property
    def color(self) -> Tuple[int,int,int]:
        return self.material.color[0] if !self.tType == TiltType.WATER else (0,63,255)
    @property
    def background(self) -> Tuple[int,int,int]:
        return self.material.color[1]
    pass