from enum import Enum
from typing import Tuple

class MaterialType(Enum):
    SOIL = 0
    STONE = 1
    CRYSTAL = 2
    WOOD = 3
    METAL = 4
    LEAVES = 5
    pass

class Material(object):
    """A material from which buildings or weapons can be made"""
    def __init__(self, mtype: MaterialType, color: Tuple[int,int,int] = (255,255,255), background: Tuple[int,int,int] = (0,0,0), sharpness: float = 0, weight: float = 1, value: float = 1, transparent: bool = False):
        self.matType = mtype
        self.color = (color,background)
        self.sharpness = sharpness
        self.weight = weight
        self.value = value
        self.transparent = transparent
        pass