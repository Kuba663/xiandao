import tcod
from tcod.console import Console
from typing import Tuple

class Object(object):
    """Generic game object"""
    def __init__(self, x : int, y : int, char: str, color : Tuple[int,int,int]):
        self.x = x
        self.y = y
        self.char = char
        self.color = color
        pass
    def move(self, dx, dy):
        self.x += dx
        self.y += dy
        pass
    def draw(self, console: Console):
        console.print(x=self.x,y=self.y,string=self.char,fg=self.color,bg=console.default_bg)
        pass
    def clear(self, console: Console):
        console.print(self.x,self.y,' ',console.default_bg)
        pass
    pass
