import numpy as np
from Xiandao import Material, Tile, materialRegistry
import math
from typing import Tuple, Union
from tcod.console import Console
from tcod.map import compute_fov
from tcod import FOV_SYMMETRIC_SHADOWCAST

class Map(object):
    """The game map"""
    def __init__(self, width: int,height: int,length: int,curr_layer=math.floor(length/2)):
        self.dimension = (width,length,height)
        self.tilemap = np.ndarray(shape=(width,length,height),dtype=int,order='C')
        self.materialmap = np.ndarray(shape=(width,length,height),dtype=int,order='C')
        self.curr_layer = curr_layer
        pass
    def __getitem__(self, index : Union[Tuple[int,int,int],Tuple[int,int]]) -> Tile.Tile:
        if type(index) == Tuple[int,int,int]:
            return Tile.Tile(self.tilemap[index[0],index[1],index[2]],materialRegistry[self.materialmap[index[0],index[1],index[2]]])
        elif type(index) == Tuple[int,int]:
            return Tile.Tile(self.tilemap[index[0],index[1],self.currentLayer],materialRegistry[self.materialmap[index[0],index[1],self.currentLayer]])
        else:
            raise IndexError()
    def __setitem__(self,index: Tuple[int,int,int], value: Tile.Tile):
        self.tilemap[index[0],index[1],index[2]] = value.tType
        self.materialmap[index[0],index[1],index[2]] = materialRegistry.index(value.material)
        pass
    def __len__(self) -> int:
        return self.dimension[0] * self.dimension[1] * self.dimension[2]
    @property
    def currentLayer(self) -> int:
        return self.curr_layer
    @currentLayer.setter
    def set_currentLayer(self, value: int):
        self.curr_layer = value
        pass
    def render(self, console: Console, pov: Tuple[int,int]):
        visible = compute_fov(self.transparency,pov,algorithm=FOV_SYMMETRIC_SHADOWCAST)
        for x in range(0,self.dimension[0]-1):
            for y in range(0,self.dimension[1]-1):
                if visible[x,y]:
                    char = self[(x,y)].char
                    color: Tuple[int,int,int] = console.default_fg
                    bg: Tuple[int,int,int] = console.default_bg
                    if self[(x,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                        color = self[(x,y)].color
                        bg = self[(x,y)].background
                        if self[(x-1,y)].tType == Tile.TileType.WALL_ARTIFICIAL and self[(x+1,y)].tType == Tile.TileType.WALL_ARTIFICIAL and self[(x,y-1)].tType == Tile.TileType.WALL_ARTIFICIAL and self[(x,y+1)].tType == Tile.TileType.WALL_ARTIFICIAL:
                            char = '╬'
                        elif self[(x,y+1)].tType == Tile.TileType.WALL_ARTIFICIAL:
                            if self[(x,y+1)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                if self[(x-1,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                    char = '╣'
                                elif self[(x+1,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                    char = '╠'
                                else:
                                    char = '║'
                            elif self[(x+1,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                char = '╔'
                            elif self[(x-1,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                char = '╗'
                            else:
                                char = '╥'
                        elif self[(x,y-1)].tType == Tile.TileType.WALL_ARTIFICIAL:
                            if self[(x+1,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                char = '╚'
                            elif self[(x-1,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                char = '╝'
                            else:
                                char = '╨'
                        elif self[(x-1,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                            if self[(x+1,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                if self[(x,y+1)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                    char = '╦'
                                elif self[(x,y-1)].tType == Tile.TileType.WALL_ARTIFICIAL:
                                    char = '╩'
                                else:
                                    char = '═'
                            else:
                                char = '╡'
                        elif self[(x+1,y)].tType == Tile.TileType.WALL_ARTIFICIAL:
                            char = '╞'
                        else:
                            char = 'O'
                            pass
                    elif self[(x,y)].tType == Tile.TileType.NONE:
                        if self[(x,y,self.currentLayer-1)].tType == Tile.TileType.NONE:
                            char = '▓'
                            color = (0,255,255)
                        else:
                            char = '.'
                            color = self[(x,y,self.currentLayer-1)].color
                            pass
                    else:
                        char = self[(x,y)].char
                        color = self[(x,y)].color
                        bg = self[(x,y)].background
                        pass
                    console.print(x,y,char,color,bg)
                    pass
                pass
            pass
        pass
    @property
    def transparency(self) -> np.ndarray:
        tArray = np.ones((self.dimension[0],self.dimension[1]),dtype=bool)
        for x in range(0,self.dimension[0]-1):
            for y in range(0,self.dimension[1]-1):
                if self[(x,y)].tType == Tile.TileType.INACCESIBLE or ((self[(x,y)].tType == Tile.TileType.WALL_ARTIFICIAL or self[(x,y)].tType == Tile.TileType.WALL_NATURAL) and not self[(x,y)].material.transparent):
                    tArray[x,y] = False
                    pass
                pass
            pass
        return tArray